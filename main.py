import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import re
import ssl
 
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
 
#Get the full website
url = "https://www.rmf.fm/r/pesel-2019.html"
response = requests.get(url, verify=False)
soup = BeautifulSoup(response.content, 'html.parser')
 
#Extract pesel numbers
xbig = soup.find(class_="xbig")
xlit = xbig.find_all(class_="xlit")
to_str = str(xlit)
list = re.findall('[0-9]+', to_str)
rmf_pesel = ''.join(map(str,list))
my_pesel = 95050312349
 
#Compare pesel numbers with mine and print result
if my_pesel == rmf_pesel:
    print (bad pesel)
else:
    print (good pesel)

